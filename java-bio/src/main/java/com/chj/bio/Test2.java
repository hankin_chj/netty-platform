package com.chj.bio;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Test2 {

    public static void main(String[] args) {
        try{
            Random random = new Random();
            int totalNum = 10000;
            String num = "11";
            double totalLotteryValue = 0; // 定义用户中奖总价值
//            float totalLotteryValue = 0; // 定义用户中奖总价值
            for(int i=0; i<5; i++){
                totalLotteryValue += Double.parseDouble(num);
            }
            totalLotteryValue = Math.ceil(totalLotteryValue);
            if(totalLotteryValue < Double.parseDouble("1000")){
                System.out.println("totalLotteryValue==="+totalLotteryValue);
            }

        }finally {
            System.out.println("totalLotteryValue===");
        }

        // 一、其实很简单：
        Double dou = 3000.533;
        dou = (double)Math.round(dou*10)/10;
        System.out.println("dou==="+dou);

       // 其中dou是变量，很简单吧，我们大多数人都知道Math.round()方法，可是我却没想到保留两位小数就是先乘以100再除以100.同理保留N位小数就不用说了吧
        // 二、第二种方法
        double d = 111231.5585;
        BigDecimal b = new BigDecimal(d);
        double df = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out.println("df==="+df);
        // 三、第三种方法
        double a = 10000; //要用double型，要不a/b永远得0;
        double q = 20000;
        NumberFormat nbf=NumberFormat.getInstance();
        nbf.setMinimumFractionDigits(2);
        String str = nbf.format(a/q);
        System.out.println("str==="+str);
    }

}
