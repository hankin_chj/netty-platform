package com.chj.rpc.server;

import com.chj.rpc.service.StockService;
import com.chj.rpc.service.impl.StockServiceImpl;

/**
 * rpc的服务端，提供服务
 */
public class StockRpcServer {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            public void run() {
                try{
                    RpcServerFrame serviceServer = new RpcServerFrame(9528);
                    serviceServer.registerSerive(StockService.class.getName(), StockServiceImpl.class);
                    serviceServer.startService();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
