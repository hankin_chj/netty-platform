package com.chj.rpc.server;

import com.chj.rpc.service.SendSms;
import com.chj.rpc.service.impl.SendSmsImpl;

import java.io.IOException;

/**
 * rpc的服务端，提供短信服务
 */
public class SmsRpcServer {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    RpcServerFrame rpcServer = new RpcServerFrame(9527);
                    rpcServer.registerSerive(SendSms.class.getName(), SendSmsImpl.class);
                    rpcServer.startService();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
