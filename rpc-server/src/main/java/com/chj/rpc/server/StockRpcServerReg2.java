package com.chj.rpc.server;

import com.chj.rpc.service.StockService;
import com.chj.rpc.service.impl.StockServiceImpl;

public class StockRpcServerReg2 {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            public void run() {
                try{
                    RpcServerFrameReg serviceServer = new RpcServerFrameReg(9192);
                    serviceServer.registerSerive(StockService.class, StockServiceImpl.class);
                    serviceServer.startService();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
