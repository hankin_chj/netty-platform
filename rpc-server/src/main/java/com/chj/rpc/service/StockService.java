package com.chj.rpc.service;

/**
 * 变动库存服务接口
 */
public interface StockService {
    void addStock(String goodsId, int addAmout);
    void reduceStock(String goodsId, int reduceAmout);
}
