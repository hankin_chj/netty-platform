package com.chj.rpc.service.impl;


import com.chj.rpc.service.StockService;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 库存数量变动服务的实现
 */
public class StockServiceImpl implements StockService {

	//存放库存数据
    private static ConcurrentHashMap<String,Integer> goodsData = new ConcurrentHashMap<String, Integer>();

    static {
        goodsData.put("A001",1000);
        goodsData.put("B002",2000);
        goodsData.put("C003",3000);
        goodsData.put("D004",4000);
    }

    @Override
    public synchronized void addStock(String goodsId, int addAmout) {
        System.out.println("+++++++++++++++++增加商品："+goodsId+"的库存,数量为："+addAmout);
        int amount = goodsData.get(goodsId)+addAmout;
        goodsData.put(goodsId,amount);
        System.out.println("+++++++++++++++++商品："+goodsId+"的库存,数量增加后变为："+amount);
    }

    @Override
    public synchronized void reduceStock(String goodsId, int reduceAmout) {
        System.out.println("-------------------减少商品："+goodsId+"的库存,数量为："+ reduceAmout);
        int amount = goodsData.get(goodsId)- reduceAmout;
        goodsData.putIfAbsent(goodsId,amount);
        System.out.println("-------------------商品："+goodsId+"的库存,减少后数量变为："+amount);
    }
}
