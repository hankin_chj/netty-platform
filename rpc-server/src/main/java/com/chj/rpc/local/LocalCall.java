package com.chj.rpc.local;

import com.chj.rpc.service.NormalBusi;
import com.chj.rpc.service.SendSms;
import com.chj.rpc.service.StockService;
import com.chj.rpc.service.impl.SendSmsImpl;
import com.chj.rpc.service.impl.StockServiceImpl;
import com.chj.rpc.vo.UserInfo;

/**
 * 本地方法调用的实现
 */
public class LocalCall {
    public static void main(String[] args) {
        NormalBusi normalBusi = new NormalBusi();
        normalBusi.business();

        SendSms sendSms = new SendSmsImpl();
        UserInfo userInfo = new UserInfo("Hankin","15691805634");
        System.out.println("Send mail: "+ sendSms.sendMail(userInfo));

        StockService stockService = new StockServiceImpl();
        stockService.addStock("A001",100);
        stockService.reduceStock("B002",50);


    }
}
