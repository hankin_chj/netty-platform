package com.chj.rpc.local;

import com.chj.rpc.service.NormalBusi;
import com.chj.rpc.service.SendSms;
import com.chj.rpc.service.StockService;
import com.chj.rpc.service.impl.SendSmsImpl;
import com.chj.rpc.service.impl.StockServiceImpl;
import com.chj.rpc.vo.UserInfo;

/**
 * 引入线程后的实现
 */
public class ThreadCall {
    public static void main(String[] args) {
        NormalBusi normalBusi = new NormalBusi();
        normalBusi.business();
        new Thread(new StockTask()).start();
        new Thread(new SmsTask()).start();
    }

    private static class StockTask implements Runnable{
        @Override
        public void run() {
            StockService stockService = new StockServiceImpl();
            stockService.addStock("A001",1000);
            stockService.reduceStock("B002",50);
        }
    }
    private static class SmsTask implements Runnable{
        @Override
        public void run() {
            SendSms sendSms = new SendSmsImpl();
            UserInfo userInfo = new UserInfo("Hankin","15691805634");
            System.out.println("Send mail: "+ sendSms.sendMail(userInfo));
        }
    }

}
