package com.chj.buffer;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufProcessor;

import java.nio.ByteBuffer;

/**
 * Buffer方法演示
 */
public class BufferMethod {
    public static void main(String[] args) {

//        ByteBuf byteBuf = ...;
//        int index = byteBuf.forEachByte(ByteBufProcessor.FIND_CR);

        System.out.println("------Test get-------------");
        ByteBuffer buffer = ByteBuffer.allocate(32);
        buffer.put((byte) 'a').put((byte) 'b').put((byte) 'c').put((byte) 'd').put((byte) 'e').put((byte)'f');
        System.out.println("before flip()" + buffer);
        // 转换为读取模式
        buffer.flip();
        System.out.println("before get():" + buffer); // before get():[pos=0 lim=5 cap=32]
        System.out.println((char) buffer.get()); // a
        System.out.println("after get():" + buffer); //after get():[pos=1 lim=5 cap=32]

        // get(index)不影响position的值
        System.out.println((char) buffer.get(2));
        System.out.println("after get(index):" + buffer); // [pos=1 lim=5 cap=32]
        System.out.println("*******************************************************************");

        byte[] dst = new byte[10];
        // position移动两位
        buffer.get(dst,0,2);
        System.out.println("after get(dst, 0, 2):" + buffer); // [pos=3 lim=6 cap=32]
        System.out.println("dst:" + new String(dst)); // dst:bc

        System.out.println("--------Test put-------");
        ByteBuffer bb = ByteBuffer.allocate(32);
        System.out.println("before put(byte):" + bb); // [pos=0 lim=32 cap=32]
        System.out.println("after put(byte):" + bb.put((byte) 'z')); // [pos=1 lim=32 cap=32]

        // put(2,(byte) 'c')不改变position的位置
        bb.put(2,(byte) 'c');
        System.out.println("after put(2,(byte) 'c'):" + bb); // [pos=1 lim=32 cap=32]
        System.out.println("after put(2,(byte) 'c')==="+new String(bb.array())); // ===z c

        // 这里的buffer是 abcdef[pos=3 lim=6 cap=32]
        bb.put(buffer);
        System.out.println("after put(buffer):" + bb); // [pos=4 lim=32 cap=32]
        System.out.println(new String(bb.array())); // zdef
        System.out.println("*******************************************************************");
        System.out.println("--------Test reset----------");
        buffer = ByteBuffer.allocate(20);
        System.out.println("buffer = " + buffer); // [pos=0 lim=20 cap=20]
        buffer.clear();
        System.out.println("*******************************************************************");
        buffer.position(5);//移动position到5
        buffer.mark();//记录当前position的位置
        buffer.position(10);//移动position到10
        System.out.println("before reset:" + buffer); // [pos=10 lim=20 cap=20]
        buffer.reset();//复位position到记录的地址
        System.out.println("after reset:" + buffer); // [pos=5 lim=20 cap=20]

        System.out.println("--------Test rewind--------");
        buffer.clear();
        buffer.position(10);//移动position到10
        buffer.limit(15);//限定最大可写入的位置为15
        System.out.println("before rewind:" + buffer); //[pos=10 lim=15 cap=20]
        buffer.rewind();//将position设回0
        System.out.println("before rewind:" + buffer); // [pos=0 lim=15 cap=20]

        System.out.println("--------Test compact--------");
        buffer.clear();
        //放入4个字节，position移动到下个可写入的位置，也就是4
        buffer.put("abcd".getBytes());
        System.out.println("before compact:" + buffer); // [pos=4 lim=20 cap=20]
        System.out.println(new String(buffer.array())); // abcd
        buffer.flip();//将position设回0，并将limit设置成之前position的值
        System.out.println("after flip:" + buffer); // [pos=0 lim=4 cap=20]
        //从Buffer中读取数据的例子，每读一次，position移动一次
        System.out.println((char) buffer.get()); // a
        System.out.println((char) buffer.get()); // b
        System.out.println((char) buffer.get()); // c
        System.out.println("after three gets:" + buffer); // [pos=3 lim=4 cap=20]
        System.out.println(new String(buffer.array())); // abcd

        // compact()方法将所有未读的数据拷贝到Buffer起始处。
        // 然后将position设到最后一个未读元素正后面。
        buffer.compact(); // d未读取，故而将d复制到起始处 字符串变为：dbcd
        System.out.println("after compact:" + buffer); // [pos=1 lim=20 cap=20]
        System.out.println(new String(buffer.array())); // dbcd


    }

}
