package com.chj.rpc.client;

import com.chj.rpc.service.SendSms;
import com.chj.rpc.service.StockService;
import com.chj.rpc.vo.UserInfo;

public class RpcClientReg {
    public static void main(String[] args) {
        UserInfo userInfo = new UserInfo("hankin","hankin@qq.com");
        SendSms sendSms = RpcClientFrameReg.getRemoteProxyObj(SendSms.class);
        System.out.println("Send mail: "+ sendSms.sendMail(userInfo));

        StockService stockService = RpcClientFrameReg.getRemoteProxyObj(StockService.class);
        stockService.addStock("A001",1000);
        stockService.reduceStock("B002",50);

    }
}
