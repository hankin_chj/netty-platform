package com.chj.rpc.client;

import com.chj.rpc.service.SendSms;
import com.chj.rpc.service.StockService;
import com.chj.rpc.vo.UserInfo;

/**
 * rpc的客户端，调用远端服务
 */
public class RpcClient {
    public static void main(String[] args) {

        UserInfo userInfo = new UserInfo("Hankin","15691805634");

        SendSms sendSms = RpcClientFrame.getRemoteProxyObj(SendSms.class,"127.0.0.1",9527);
        System.out.println("Send mail: "+ sendSms.sendMail(userInfo));

        StockService stockService = RpcClientFrame.getRemoteProxyObj(StockService.class,"127.0.0.1",9528);
        stockService.addStock("A001",100);
        stockService.reduceStock("B002",50);

//        StockService stockService2 = new StockServiceImpl();
//        stockService.addStock("A001",1000);
//        stockService.deduceStock("B002",50);

    }
}
