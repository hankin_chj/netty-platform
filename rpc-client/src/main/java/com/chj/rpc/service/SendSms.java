package com.chj.rpc.service;

import com.chj.rpc.vo.UserInfo;

/**
 * 短信息发送接口
 */
public interface SendSms {

    boolean sendMail(UserInfo user);

}
