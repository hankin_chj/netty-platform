package com.chj.proxy.normal;


import com.chj.proxy.IGetServant;
import com.chj.proxy.Receptionist;

/**
 */
public class Lison {

    public static void main(String[] args) {
        IGetServant getServant = new Receptionist();
        getServant.choice("御姐范风格");

//        IGetServant james = new James();
//        james.choice("喜欢的样子");
    }

}
