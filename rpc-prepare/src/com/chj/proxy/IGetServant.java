package com.chj.proxy;

/**
 *类说明：选择服务接口
 */
public interface IGetServant {
    void choice(String desc);
}
