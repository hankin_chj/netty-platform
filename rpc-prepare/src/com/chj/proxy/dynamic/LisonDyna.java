package com.chj.proxy.dynamic;


import com.chj.proxy.IGetServant;
import com.chj.proxy.Receptionist;

import java.lang.reflect.Proxy;

public class LisonDyna {
    public static void main(String[] args) {
        IGetServant james =(IGetServant) Proxy.newProxyInstance(IGetServant.class.getClassLoader(),
                        new Class[]{IGetServant.class}, new JamesDyna(new Receptionist()));
        james.choice("御姐范风格");
    }
}
